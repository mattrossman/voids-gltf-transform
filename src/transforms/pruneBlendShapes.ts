import type { Transform } from "@gltf-transform/core"

export function pruneBlendShapes(): Transform {
  return doc => {
    const root = doc.getRoot()
    const meshes = root.listMeshes()

    for (const mesh of meshes) {
      const primitives = mesh.listPrimitives()

      /**
       * Morph target names are not officialy in the glTF spec
       * https://github.com/KhronosGroup/glTF/issues/1036
       *
       * We need to manually update mesh.extras and mesh.weights to reflect new shapes
       */
      const extras = mesh.getExtras()
      const targetNames = extras.targetNames as string[]
      const weights = mesh.getWeights()

      for (const primitive of primitives) {
        const targets = primitive.listTargets()

        for (const target of targets) {
          const targetName = target.getName()
          if (!allowedBlendShapes.has(targetName)) {
            target.dispose()
            const i = targetNames.indexOf(targetName)
            targetNames.splice(i, 1)
            weights.splice(i, 1)
          }
        }
      }
      mesh.setExtras({ ...extras, targetNames })
      mesh.setWeights(weights)
    }
  }
}

const allowedBlendShapes = new Set([
  "browOuterUpRight",
  "browOuterUpLeft",
  "mouthUpperUpRight",
  "mouthUpperUpLeft",
  "mouthStretchRight",
  "mouthStretchLeft",
  "mouthSmileRight",
  "mouthSmileLeft",
  "mouthShrugUpper",
  "mouthShrugLower",
  "mouthRollUpper",
  "mouthRollLower",
  "mouthRight",
  "mouthPucker",
  "mouthPressRight",
  "mouthPressLeft",
  "mouthLowerDownRight",
  "mouthLowerDownLeft",
  "mouthLeft",
  "mouthFunnel",
  "mouthFrownRight",
  "mouthFrownLeft",
  "mouthDimpleRight",
  "mouthDimpleLeft",
  "mouthClose",
  "jawRight",
  "jawOpen",
  "jawLeft",
  "jawForward",
  "eyeWideRight",
  "eyeWideLeft",
  "eyeSquintRight",
  "eyeSquintLeft",
  "eyeLookUpRight",
  "eyeLookUpLeft",
  "eyeLookOutRight",
  "eyeLookOutLeft",
  "eyeLookInRight",
  "eyeLookInLeft",
  "eyeLookDownRight",
  "eyeLookDownLeft",
  "eyeBlinkRight",
  "eyeBlinkLeft",
  "cheekSquintRight",
  "cheekSquintLeft",
  "cheekPuffRight",
  "cheekPuffLeft",
  "browOuterRight",
  "browOuterLeft",
  "browInnerUp",
  "browDownRight",
  "browDownLeft",
])
