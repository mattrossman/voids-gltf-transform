import os from "os"
import path from "path"
import fs from "fs"
import { Logger, NodeIO } from "@gltf-transform/core"
import { meshopt, prune, webp } from "@gltf-transform/functions"
import { MeshoptCompression } from "@gltf-transform/extensions"

import { MeshoptEncoder } from "meshoptimizer"

// @ts-ignore
import * as squoosh from "@squoosh/lib"

import { formatBytes } from "./util.js"
import { pruneBlendShapes } from "./transforms/pruneBlendShapes.js"

const INPUT_DIR = "input"
const OUTPUT_DIR = "output"

/**
 * References:
 * https://github.khronos.org/glTF-Tutorials/gltfTutorial/
 * https://github.com/donmccurdy/glTF-Transform
 * https://gltf-transform.donmccurdy.com/functions.html
 * https://gltf-transform.donmccurdy.com/classes/extensions.meshoptcompression.html
 * https://gltf-transform.donmccurdy.com/classes/extensions.texturewebp.html
 */

await MeshoptEncoder.ready

const io = new NodeIO().registerExtensions([MeshoptCompression]).registerDependencies({
  "meshopt.encoder": MeshoptEncoder,
})

if (!fs.existsSync(OUTPUT_DIR)) fs.mkdirSync(OUTPUT_DIR, { recursive: true })
const files = fs.readdirSync(INPUT_DIR)

console.log(`Preparing to compress ${files.length} files`)

async function processFile(fileName: string) {
  const document = await io.read(path.join(INPUT_DIR, fileName))
  document.setLogger(new Logger(Logger.Verbosity.SILENT))

  const readBytes = io.lastReadBytes

  await document.transform(
    webp({ squoosh, jobs: os.cpus().length }),
    pruneBlendShapes(),
    prune(),
    meshopt({ encoder: MeshoptEncoder, level: "medium" }),
  )

  await io.write(path.join(OUTPUT_DIR, fileName), document)
  const writeBytes = io.lastWriteBytes

  console.log(`compressed ${fileName} (${formatBytes(readBytes)} → ${formatBytes(writeBytes)})`)
}

const pool = files.map(fileName => processFile(fileName))

await Promise.allSettled(pool)

console.log("Finished")
