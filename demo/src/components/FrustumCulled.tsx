import { useLayoutEffect, useRef } from "react"

export function FrustumCulled({ value = true, children }: { children: any; value?: boolean }) {
  const ref = useRef<THREE.Group>(null)
  useLayoutEffect(() => {
    if (ref.current) {
      ref.current.traverse(object => (object.frustumCulled = value))
    }
  }, [value])
  return <group ref={ref}>{children}</group>
}
