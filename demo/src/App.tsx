import { Suspense, useEffect, useLayoutEffect, useMemo, useRef, useState } from "react"
import { Backdrop, Bounds, Center, Environment, OrbitControls, Stage, TorusKnot, useGLTF } from "@react-three/drei"
import { Canvas, PrimitiveProps, useFrame } from "@react-three/fiber"
import { FrustumCulled } from "./components/FrustumCulled"
import * as THREE from "three"
import { Leva, useControls } from "leva"

export default function App() {
  return (
    <>
      <Leva />
      <Canvas>
        <color attach="background" args={["black"]} />
        <OrbitControls makeDefault />
        <FrustumCulled value={false}>
          <Void />
        </FrustumCulled>
        <Environment preset="studio" background />
      </Canvas>
    </>
  )
}

function Void(props: Omit<PrimitiveProps, "object">) {
  // const { scene, ...rest } = useGLTF("/models/void_2.glb")
  const { scene, ...rest } = useGLTF("/models/void_10z.glb")
  Object.assign(window, rest)
  const { controls } = useMemo(() => {
    const meshes: THREE.Mesh[] = []
    const morphSet = new Set<string>()
    scene.traverse(object => {
      if (
        (object instanceof THREE.SkinnedMesh || object instanceof THREE.Mesh) &&
        object?.morphTargetInfluences?.length &&
        object.morphTargetDictionary
      ) {
        Object.keys(object.morphTargetDictionary).forEach(morph => morphSet.add(morph))
        meshes.push(object)
      }
    })

    const controls = Object.fromEntries(
      Array.from(morphSet).map(morph => [
        morph,
        {
          value: 0,
          min: 0,
          max: 1,
          onChange: (influence: number) => {
            meshes.forEach(mesh => {
              const id = mesh!.morphTargetDictionary![morph] as number
              mesh.morphTargetInfluences![id] = influence
            })
          },
        },
      ]),
    )

    return { meshes, controls }
  }, [scene])

  useControls(controls)

  return <primitive dispose={null} object={scene} position={[0, -1, 0]} {...props} />
}
